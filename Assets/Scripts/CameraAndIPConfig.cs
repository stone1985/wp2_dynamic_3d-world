﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[Serializable]
public class CameraAndIPConfig {

	private static String networkAddress;
	public String NetworkAddress {
		get { return networkAddress; }
		set { networkAddress = value; }
	}

	private static Vector3 cameraPosition;
	public Vector3 CameraPosition {
		get { return cameraPosition; }
		set { cameraPosition = value; }
	}

	private static Vector3 cameraRotation;
	public Vector3 CameraRotation {
		get { return cameraRotation; }
		set { cameraRotation = value; }
	}

	public void Save(string path)
	{
        
        var serializer = new XmlSerializer(typeof(CameraAndIPConfig));        
        var encoding = System.Text.Encoding.GetEncoding("UTF-8");
        using (StreamWriter stream = new StreamWriter(path, false, encoding))
        {
            serializer.Serialize(stream, this);
        }      
                
	}

	public static CameraAndIPConfig Load(string path)
	{        
        var serializer = new XmlSerializer(typeof(CameraAndIPConfig));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as CameraAndIPConfig;
		}
        
	}
}