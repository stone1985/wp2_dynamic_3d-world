﻿using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System;
using UnityEngine.UI;
using UnityEngine.Networking.NetworkSystem;

public class PlayerController : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public GameObject ballPrefab;
    public Transform bulletSpawn;
    public Transform ballSpawn;
    public Vector3 position;
    public float myTimer = 0.5F;
    public int spawned = 0;
    public int numToSpawn = 1;
    public int start = 0;

    [SyncVar]
    public float x, z;
    [SyncVar]
    public bool networkInfoClientActive = false;
    public bool networkInfoServerActive = false;
    public bool ViewOfClientsActive = false;
    public CameraAndIPConfig saveGame;
    public NetworkManager manager;
    private float rotateSpeed = 10;
    private float speed = 10;
    public GameObject networkview;
    public Camera cam1;
	public Camera cam2;
	public Camera cam3;
    public GameObject thePlayer;
    public PlayerController playerController;
    public int cameraAngle;
	public int cameraAngle2;
	public int cameraAngle3;
    public bool cameraAngleSet = false;
	public bool cameraAngleSet2 = false;
	public bool cameraAngleSet3 = false;

    void Start()
    {
		//Debug.Log(Application.dataPath);
        NetworkServer.RegisterHandler(1000, OnLoginRequest); // 1000 -> your own short message type
		NetworkServer.RegisterHandler(1001, OnLoginRequest2); // 1001 -> your own short message type
		NetworkServer.RegisterHandler(1002, OnLoginRequest3); // 1002 -> your own short message type
    }

    void OnLoginRequest(NetworkMessage msg)
    {
		Debug.Log ("OnLoginRequest");
        int value = msg.ReadMessage<IntegerMessage>().value;
        Debug.Log(value);
        try
        {
            cameraAngle = value;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        cameraAngleSet = true;
    }

	void OnLoginRequest2(NetworkMessage msg)
	{
		Debug.Log ("OnLoginRequest2");
		int value2 = msg.ReadMessage<IntegerMessage>().value;
		Debug.Log(value2);
		try
		{
			cameraAngle2 = value2;
		}
		catch (Exception e)
		{
			Debug.Log(e);
		}
		cameraAngleSet2 = true;
	}

	void OnLoginRequest3(NetworkMessage msg)
	{
		Debug.Log ("OnLoginRequest3");
		int value3 = msg.ReadMessage<IntegerMessage>().value;
		Debug.Log(value3);
		try
		{
			cameraAngle3 = value3;
		}
		catch (Exception e)
		{
			Debug.Log(e);
		}
		cameraAngleSet3 = true;
	}

    void Update()
    {
        if (ViewOfClientsActive == true)
        {
			//cam 1
            GameObject.Find("CamClient1").GetComponent<Camera>().enabled = true;
            GameObject.Find("CamClient1").GetComponent<Camera>().rect = new Rect(0.8f, 0.8f, 0.2f, 0.2f);
            thePlayer = GameObject.Find("Player(Clone)");
            playerController = (PlayerController)thePlayer.GetComponent<PlayerController>();
            cam1 = GameObject.Find("CamClient1").GetComponent<Camera>();

            if (cameraAngleSet == true)
            {
                cam1.transform.rotation *= Quaternion.Euler(0, cameraAngle, 0);
                Debug.Log("Camera angle set");
                cameraAngleSet = false;
            }

            cam1.transform.Rotate(0, playerController.x, 0);
			Vector3 newPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + 3, thePlayer.transform.position.z);
            cam1.transform.position = newPos;


			//cam 2
            GameObject.Find("CamClient2").GetComponent<Camera>().enabled = true;
            GameObject.Find("CamClient2").GetComponent<Camera>().rect = new Rect(0.8f, 0.6f, 0.2f, 0.2f);
			cam2 = GameObject.Find("CamClient2").GetComponent<Camera>();

			if (cameraAngleSet2 == true)
			{
				cam2.transform.rotation *= Quaternion.Euler(0, cameraAngle2, 0);
				Debug.Log("Camera angle set2");
				cameraAngleSet2 = false;
			}

			cam2.transform.Rotate(0, playerController.x, 0);
			Vector3 newPos2 = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + 3, thePlayer.transform.position.z);
			cam2.transform.position = newPos2;


			//cam 3
			GameObject.Find("CamClient3").GetComponent<Camera>().enabled = true;
			GameObject.Find("CamClient3").GetComponent<Camera>().rect = new Rect(0.8f, 0.4f, 0.2f, 0.2f);
			cam3 = GameObject.Find("CamClient3").GetComponent<Camera>();

			if (cameraAngleSet3 == true)
			{
				cam3.transform.rotation *= Quaternion.Euler(0, cameraAngle3, 0);
				Debug.Log("Camera angle set3");
				cameraAngleSet3 = false;
			}

			cam3.transform.Rotate(0, playerController.x, 0);
			Vector3 newPos3 = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + 3, thePlayer.transform.position.z);
			cam3.transform.position = newPos3;
        }
        else
        {
            try
            {
                GameObject.Find("CamClient1").GetComponent<Camera>().enabled = false;
                GameObject.Find("CamClient2").GetComponent<Camera>().enabled = false;
				GameObject.Find("CamClient3").GetComponent<Camera>().enabled = false;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        if (networkInfoServerActive == true)
        {
            //count wird nicht vermindert, wenn Verbindung getrennt wird
            int count = NetworkServer.connections.Count;
            GameObject.Find("ConnectedClients").GetComponent<Text>().text = "Connected Clients: " + count + System.Environment.NewLine;

            for (int i = 0; i < NetworkServer.connections.Count; ++i)
            {
                NetworkConnection c = NetworkServer.connections[i];
                if (c == null)
                {
                    continue;
                }
                byte error;
                int rtt = NetworkTransport.GetCurrentRtt(c.hostId, c.connectionId, out error);
            }

            foreach (NetworkConnection n in NetworkServer.connections)
            {
                if (n == null) continue;
                byte error;
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "Connection ID: ";
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += n.connectionId.ToString() + ",";
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "IP Address: " + n.address + System.Environment.NewLine;
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "is ready: " + n.isReady + System.Environment.NewLine;
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "is connected: " + n.isConnected + System.Environment.NewLine;
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "Ping: " + NetworkTransport.GetCurrentRtt(n.hostId, n.connectionId, out error) + "ms" + System.Environment.NewLine;
                GameObject.Find("ConnectedClients").GetComponent<Text>().text += "Connection time: " + NetworkTransport.GetNetIOTimeuS() + "ms" + System.Environment.NewLine;
                //NetworkTransport.GetRemoteDelayTimeMS continue with this

                GameObject.Find("ConnectedClients").GetComponent<Text>().enabled = true;
            }
        }
        else
        {
            GameObject.Find("ConnectedClients").GetComponent<Text>().enabled = false;
        }


        if (start == 1)
        {
            if (myTimer > 0)
            {
                myTimer -= Time.deltaTime;
            }
            if (myTimer <= 0)
            {
                while (spawned < numToSpawn)
                {

                    position = new Vector3(UnityEngine.Random.Range(0.0F, 400.0F), UnityEngine.Random.Range(0.0F, 100.0F), UnityEngine.Random.Range(0.0F, 400.0F));

                    // Create the Bullet from the Bullet Prefab
                    var ball = (GameObject)Instantiate(
                        bulletPrefab,
                        position,
                        Quaternion.identity);


                    // Spawn the bullet on the Clients
                    NetworkServer.Spawn(ball);

                    Debug.Log("ball spawned");

                    // Destroy the bullet after 2 seconds
                    Destroy(ball, 20.0f);
                    myTimer = 0.5f;
                    spawned++;
                }
                spawned = 0;
            }
        }
        if (!isLocalPlayer)
        {
            return;
        }

        x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        z = Input.GetAxis("Vertical") * Time.deltaTime * 15.0f;

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SpawnBall();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            CmdInfo();
        }
        if (Input.GetKeyDown(KeyCode.N))
        {
            NetworkInfo();
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            ViewOfClients();
        }
    }

    void ViewOfClients()
    {
        if (ViewOfClientsActive == false)
        {
            ViewOfClientsActive = true;
        }
        else
        {
            ViewOfClientsActive = false;
        }
    }

    void NetworkInfo()
    {
        if (networkInfoServerActive == false)
        {
            networkInfoServerActive = true;
        }
        else
        {
            networkInfoServerActive = false;
        }
    }

    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdInfo()
    {
        if (networkInfoClientActive == false)
        {
            networkInfoClientActive = true;
        }
        else
        {
            networkInfoClientActive = false;
        }
    }



    void SpawnBall()
    {
        start = 1;
        UpdateView();
    }

    // This [Command] code is called on the Client …
    // … but it is run on the Server!
    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // Spawn the bullet on the Clients
        NetworkServer.Spawn(bullet);

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
        GameObject.Find("ServerText").GetComponent<Text>().enabled = true;
        GameObject.Find("ConnectedClients").GetComponent<Text>().enabled = true;
    }
    public void UpdateView()
    {

        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
        }
        if (myTimer <= 0)
        {
            while (spawned < numToSpawn)
            {

                position = new Vector3(UnityEngine.Random.Range(0.0F, 200.0F), UnityEngine.Random.Range(0.0F, 100.0F), UnityEngine.Random.Range(50.0F, 80.0F));

                // Create the Bullet from the Bullet Prefab
                var ball = (GameObject)Instantiate(
                             bulletPrefab,
                             position,
                             Quaternion.identity);

                // Spawn the bullet on the Clients
                NetworkServer.Spawn(ball);

                Debug.Log("ball spawned");

                // Destroy the bullet after 2 seconds
                Destroy(ball, 20.0f);
                myTimer = 2;
                spawned++;
            }
            spawned = 0;
        }

    }
}