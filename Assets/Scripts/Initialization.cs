﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class Initialization : MonoBehaviour {

    public CameraAndIPConfig cameraAndIPConfig;
    public NetworkManager networkManager;
    //public Text NetworkInfoText;


    // Use this for initialization
    void Start () {
        GameObject.Find("NetworkInfoText").GetComponent<Text>().enabled = false;
        /*
        //save        
        try
        {
            cameraAndIPConfig.CameraPosition = GameObject.Find("MainCamera").transform.position;
            cameraAndIPConfig.CameraRotation = GameObject.Find("MainCamera").transform.eulerAngles;
            networkManager = GameObject.FindObjectOfType<NetworkManager>();
            Debug.Log(networkManager.networkAddress);
            cameraAndIPConfig.NetworkAddress = networkManager.networkAddress;
            cameraAndIPConfig.Save(Path.Combine(Application.dataPath, "config.xml"));
            Debug.Log(Application.dataPath + "/config.xml");
        }
        catch(Exception e)
        {
            Debug.Log(e);
        }        
        */

        //load
        try
        {            
            cameraAndIPConfig = CameraAndIPConfig.Load(Path.Combine(Application.dataPath, "config.xml")); 
            GameObject.Find("CamClient1").transform.position = cameraAndIPConfig.CameraPosition;
            GameObject.Find("CamClient1").transform.eulerAngles = cameraAndIPConfig.CameraRotation;
            GameObject.FindObjectOfType<NetworkManager>().networkAddress = cameraAndIPConfig.NetworkAddress;
        }
        catch(Exception e)
        {
            Debug.Log(e);
        } 
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
