﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking.NetworkSystem;

public class NetworkViewScript3 : NetworkBehaviour
{
	public GameObject thePlayer;
	public NetworkManagerHUD networkManagerHUD;
	public PlayerController playerController;
	public PlayerController playerControllerForNetworkInfoText;
	public Camera mainCam;
	public CameraAndIPConfig cameraAndIPConfig;
	private NetworkClient nClient;

	[SyncVar]
	public float cameraAngle3;

	// Use this for initialization
	void Start()
	{
		InitializePlayer();
		//load
		try
		{
			cameraAndIPConfig = CameraAndIPConfig.Load(Path.Combine(Application.dataPath, "config.xml"));
		}
		catch (Exception e)
		{
			Debug.Log(e);
		}

		Vector3 newPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y, thePlayer.transform.position.z);
		mainCam.transform.position = newPos;
		Quaternion newRot = new Quaternion(thePlayer.transform.rotation.x, thePlayer.transform.rotation.y, thePlayer.transform.rotation.z, thePlayer.transform.rotation.w);
		mainCam.transform.rotation = newRot;
		cameraAngle3 = cameraAndIPConfig.CameraRotation.y;
		mainCam.transform.rotation *= Quaternion.Euler(0, cameraAngle3, 0);
		nClient.Send(1002, new IntegerMessage((int)(cameraAngle3)));
		//networkManagerHUD.enabled = false;
	}


	private void InitializePlayer()
	{
		GameObject.Find("ClientText").GetComponent<Text>().enabled = true;
		networkManagerHUD = GameObject.FindObjectOfType<NetworkManagerHUD>();
		nClient = GameObject.FindObjectOfType<NetworkManager>().client;
		Debug.Log("Player connected");
		thePlayer = GameObject.Find("Player(Clone)");
		if (thePlayer == null)
		{
			thePlayer = GameObject.Find("Player");
		}
		Debug.Log(thePlayer);
		if (thePlayer != null)
		{
			playerController = (PlayerController)thePlayer.GetComponent<PlayerController>();
			Debug.Log("playerController instantiated");
		}

		mainCam = GameObject.Find("CamClient3").GetComponent<Camera>();

		GameObject.Find("CamClient1").GetComponent<Camera>().enabled = false;
		GameObject.Find("CamClient2").GetComponent<Camera>().enabled = false;

	}   


	// Update is called once per frame
	void Update()
	{
		if (playerController != null)
		{
			if (playerController.networkInfoClientActive == true)
			{
				int latency = nClient.GetRTT();
				String ip = Network.player.ipAddress;
				GameObject.Find("NetworkInfoText").GetComponent<Text>().text = "Netzwerk Informationen" + System.Environment.NewLine;
				GameObject.Find("NetworkInfoText").GetComponent<Text>().text += "IP Adresse: " + ip;
				GameObject.Find("NetworkInfoText").GetComponent<Text>().text += System.Environment.NewLine + "Latenz: " + latency + "ms";
				GameObject.Find("NetworkInfoText").GetComponent<Text>().text += System.Environment.NewLine + "Kamerawinkel: " + (int)(GameObject.Find("MainCamera").transform.rotation.y * 100) + "°";
				GameObject.Find("NetworkInfoText").GetComponent<Text>().enabled = true;
			}
			else
			{
				GameObject.Find("NetworkInfoText").GetComponent<Text>().enabled = false;
			}
			if (mainCam == null)
			{
				mainCam = GameObject.Find("CamClient3").GetComponent<Camera>();
				Debug.Log("Camera reinstantiated");
			}

			mainCam.transform.Rotate(0, playerController.x, 0);
			Vector3 newPos = new Vector3(thePlayer.transform.position.x, thePlayer.transform.position.y + cameraAndIPConfig.CameraPosition.y, thePlayer.transform.position.z);
			mainCam.transform.position = newPos;

		}
		else
		{
			InitializePlayer();
		}

	}
}
